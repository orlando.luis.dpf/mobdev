# DESAFIO MOBDEV
- - -

1. Objetivo:
   - Consumir los metodos get de la api de Rick and Morty

2. Tecnologías
    ```
    * Spring Boot 2.4.3
    * RestTemplate
    * slf4j
    * springdoc - swagger
    * Java 16
    * Mockito - Junit ( 3.6.28)
    ```

- Para probar la llamada a los metodos, utilizar el archivo de postman: 
```rickandmorty.postman_collection.json```

[![View in Swagger](http://localhost:8080/open-api/swagger-ui/index.html#/location-controller/location)](http://localhost:8080/open-api/swagger-ui/index.html#/location-controller/location)

Desarrollado por: Orlando del Pozo Florez 