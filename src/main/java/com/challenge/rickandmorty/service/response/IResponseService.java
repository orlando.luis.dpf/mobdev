package com.challenge.rickandmorty.service.response;

import com.challenge.rickandmorty.entity.response.Response;

public interface IResponseService {
    Response generateResponse(Long id) ;
}
