package com.challenge.rickandmorty.service.response;

import com.challenge.rickandmorty.entity.character.SingleCharacter;
import com.challenge.rickandmorty.entity.location.SingleLocation;
import com.challenge.rickandmorty.entity.response.Origin;
import com.challenge.rickandmorty.entity.response.Response;
import com.challenge.rickandmorty.ex.BadRequestException;
import com.challenge.rickandmorty.ex.NotFoundException;
import com.challenge.rickandmorty.service.character.ICharacterService;
import com.challenge.rickandmorty.service.location.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResponseServiceImpl implements IResponseService {

    @Autowired
    private ICharacterService characterService;

    @Autowired
    private ILocationService locationService;

    public Response generateResponse(Long id) {


        if ( id < 1)
            throw new BadRequestException("Error, el valor del id enviado es invalido");

        SingleCharacter singleCharacter = characterService.getCharacterForId(id);
        if (singleCharacter == null)
            throw new NotFoundException("Error singleCharacter id no existe");

        SingleLocation singleLocation = locationService.getSingleLocation(id);
        if (singleLocation == null)
            throw new NotFoundException("Error singleLocation id no existe");


        Response response = new Response();
        response.setId(singleCharacter.getId());
        response.setName(singleCharacter.getName());
        response.setStatus(singleCharacter.getStatus());
        response.setSpecies(singleCharacter.getSpecies());
        response.setType(singleCharacter.getType());
        response.setEpisode_count(singleCharacter.getEpisode().length);

        Origin origin = new Origin();
        origin.setName(singleLocation.getName());
        origin.setUrl(singleLocation.getUrl());
        origin.setDimension(singleLocation.getDimension());
        origin.setResidents(singleLocation.getResidents());
        response.setOrigin(origin);

        return response;
    }

   /* public Response generateResponse(Long id) throws ResultException {

        Response response = new Response();

        try {

            SingleCharacter singleCharacter = characterService.getCharacterForId(id);
            if (singleCharacter == null)
                return null;

            SingleLocation singleLocation = locationService.getSingleLocation(id);
            if (singleLocation != null)
                throw new ResultException("ERROR PUES");


            response.setId(singleCharacter.getId());
            response.setName(singleCharacter.getName());
            response.setStatus(singleCharacter.getStatus());
            response.setSpecies(singleCharacter.getSpecies());
            response.setType(singleCharacter.getType());
            response.setEpisode_count(singleCharacter.getEpisode().length);

            Origin origin = new Origin();
            origin.setName(singleLocation.getName());
            origin.setUrl(singleLocation.getUrl());
            origin.setDimension(singleLocation.getDimension());
            origin.setResidents(singleLocation.getResidents());
            response.setOrigin(origin);
            return response;
        }catch (Exception e){
            throw new ResultException(e.getMessage(), e);
        }






    }*/
}
