package com.challenge.rickandmorty.service.location;

import com.challenge.rickandmorty.entity.location.SingleLocation;

public interface ILocationService {
    SingleLocation getSingleLocation(Long id);
}
