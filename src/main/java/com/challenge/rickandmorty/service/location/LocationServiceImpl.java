package com.challenge.rickandmorty.service.location;


import com.challenge.rickandmorty.entity.location.SingleLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LocationServiceImpl implements ILocationService {
    private static Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);

    @Value("${url.location}")
    private String url;

    @Autowired
    private RestTemplate clientRest;

    public SingleLocation getSingleLocation(Long id) {
        try {
            String endpoint = url.concat(String.valueOf(id));
            SingleLocation singleLocation = clientRest.getForObject(endpoint, SingleLocation.class);
            logger.info("Return result getSingleLocation API =>  {}", singleLocation);
            return singleLocation;
        }catch (Exception e) {
            logger.error("Error call method api getSingleLocation  =>  {}", e);
            return null;
        }

    }

}
