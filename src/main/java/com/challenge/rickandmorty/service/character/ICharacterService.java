package com.challenge.rickandmorty.service.character;

import com.challenge.rickandmorty.entity.character.SingleCharacter;

public interface ICharacterService {
    SingleCharacter getCharacterForId(Long id);
}
