package com.challenge.rickandmorty.service.character;

import com.challenge.rickandmorty.entity.character.SingleCharacter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CharacterServiceImpl implements ICharacterService {



    private static Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);

    @Value("${url.character}")
    private String url;

    @Autowired
    private RestTemplate clientRest;

    @Override
    public SingleCharacter getCharacterForId(Long id) {
        try {
            String endpoint = url.concat(String.valueOf(id));
            SingleCharacter singleCharacter = clientRest.getForObject(endpoint, SingleCharacter.class);
            logger.info("Return result getCharacterForId API =>  {}", singleCharacter);
            return singleCharacter;
        }catch (Exception e) {
            logger.error("Error call method api getCharacterForId  =>  {}", e);
            return null;
        }
    }

}
