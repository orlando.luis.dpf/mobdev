package com.challenge.rickandmorty.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfiguration {
    @Bean("clientRest")
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
