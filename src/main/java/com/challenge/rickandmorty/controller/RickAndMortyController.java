package com.challenge.rickandmorty.controller;


import com.challenge.rickandmorty.entity.response.Response;
import com.challenge.rickandmorty.service.response.IResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/character", produces = MediaType.APPLICATION_JSON_VALUE)
public class RickAndMortyController {

    private static Logger logger = LoggerFactory.getLogger(RickAndMortyController.class);

    @Autowired
    private IResponseService responseService;


    @GetMapping("/{id}")
    // @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Response> genericInformation(@PathVariable Long id) {
        logger.info("Input character request ID => {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(responseService.generateResponse(id));
    }

}
